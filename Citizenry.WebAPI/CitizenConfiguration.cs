﻿using System;

namespace Citizenry.WebAPI
{
    public class CitizenConfiguration
    {
        public readonly string ConnectionString;
        public readonly Type CitizenRepositoryType;

        public CitizenConfiguration(string connectionString, string citizenRepositoryTypeName)
        {
            if (string.IsNullOrWhiteSpace(connectionString))
            {
                throw new ArgumentNullException(nameof(connectionString));
            }

            if (string.IsNullOrWhiteSpace(citizenRepositoryTypeName))
            {
                throw new ArgumentNullException(nameof(citizenRepositoryTypeName));
            }

            this.ConnectionString = connectionString;
            this.CitizenRepositoryType = Type.GetType(citizenRepositoryTypeName, throwOnError: true);
        }
    }
}
