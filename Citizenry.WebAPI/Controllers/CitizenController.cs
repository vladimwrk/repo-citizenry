﻿using Citizenry.Domain;
using Citizenry.Domain.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Citizenry.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CitizenController : ControllerBase
    {
        private readonly ICitizenService _citizenService;

        public CitizenController(ICitizenService citizenService)
        {
            if (citizenService == null)
            {
                throw new ArgumentNullException(nameof(citizenService));
            }

            _citizenService = citizenService;
        }

        #region CRUD methods

        /// <summary>
        /// Get Citizens.
        /// </summary>
        /// <remarks>
        /// GET api/citizen
        /// </remarks>
        /// <returns>List of citizens</returns>
        [HttpGet]
        public async Task<IActionResult> GetCitizens()
        {
            try
            {
                var citizens = await _citizenService.GetAllCitizensAsync();
                return Ok(citizens);
            }
            catch (Exception ex)
            {
                //log
                return StatusCode(500, ex.Message);
            }
        }

        /// <summary>
        /// Get citizen by id.
        /// </summary>
        /// <remarks>
        /// GET api/citizen/id
        /// </remarks>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}", Name = "CitizenById")]
        public async Task<IActionResult> GetCitizenById(int id)
        {
            try
            {
                var citizen = await _citizenService.GetCitizenByIdAsync(id);
                if (citizen == null)
                {
                    return NotFound();
                }

                return Ok(citizen);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        /// <summary>
        /// Add a new citizen to the store.
        /// </summary>
        /// <remarks>
        /// POST api/citizen
        ///
        /// Content-Type: application/json;
        ///
        /// {"lastName":"Ляпкин","firstName":"Амос","middleName":"Федорович","taxpayerNumber":"0123456789",
        /// "insuranceNumber":"123-123-345-67","birthDate":"1947-05-05","deathDate":null}
        /// 
        /// </remarks>
        /// <param name="citizen"></param>
        /// <returns>New created citizen</returns>
        /// <response code="200">Return the newly created citizen</response>
        /// <response code="400">If the citizen is null</response>
        [HttpPost]
        [ProducesResponseType(typeof(Citizen), 200)]
        [ProducesResponseType(typeof(Citizen), 404)]
        public async Task<IActionResult> CreateCitizenAsync([FromBody] Citizen citizen)
        {
            try
            {
                if (citizen == null)
                {
                    return BadRequest("Invalid model object");
                }

                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }

                await _citizenService.CreateAsync(citizen);

                return CreatedAtRoute("CitizenById", new { id = citizen.Id, Controller = "Citizen" }, citizen);
            }
            catch (Exception ex)
            {
                //logger
                return StatusCode(500, ex.Message);
            }
        }

        /// <summary>
        /// Update an existing citizen.
        /// </summary>
        /// <remarks>
        /// PUT api/citizen/id
        ///
        /// Content-Type: application/json;
        ///
        /// {"lastName":"Тяпкин","firstName":"Амос","middleName":"Федорович","taxpayerNumber":"0123456777",
        /// "insuranceNumber":"123-234-345-67","birthDate":"1805-05-05","deathDate":null}
        /// 
        /// </remarks>
        /// <param name="id"></param>
        /// <param name="citizen"></param>
        /// <returns></returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateCitizen(int id, Citizen citizen)
        {
            try
            {
                if (citizen == null)
                {
                    return BadRequest("Citizen object is null.");
                }

                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }

                var citizenEntity = await _citizenService.GetCitizenByIdAsync(id);
                if (citizenEntity == null)
                {
                    return NotFound();
                }

                citizenEntity.LastName = citizen.LastName;
                citizenEntity.FirstName = citizen.FirstName;
                citizenEntity.MiddleName = citizen.MiddleName;
                citizenEntity.TaxpayerNumber = citizen.TaxpayerNumber;
                citizenEntity.InsuranceNumber = citizen.InsuranceNumber;
                citizenEntity.BirthDate = citizen.BirthDate;
                citizenEntity.DeathDate = citizen.DeathDate;

                await _citizenService.UpdateAsync(citizenEntity);

                return NoContent();
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        /// <summary>
        /// Delete citizen.
        /// </summary>
        /// <remarks>
        /// DELETE api/citizen/id
        /// </remarks>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteCitizen(int id)
        {
            try
            {
                var citizen = await _citizenService.GetCitizenByIdAsync(id);
                if (citizen == null)
                {
                    return NotFound();
                }

                await _citizenService.DeleteAsync(citizen);

                return NoContent();
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        #endregion

        #region Find methods
        /// <summary>
        /// Find by name
        /// </summary>
        /// <remarks>
        /// GET api/citizen/FindByName?name=
        /// </remarks>
        /// <param name="name">name of citizen</param>
        /// <returns></returns>
        [HttpGet("FindByName")]
        public async Task<IActionResult> FindByNameAsync(string name)
        {
            return await FindingByNameAsync(name);
        }

        /// <summary>
        /// Find by insurance number
        /// </summary>
        /// <remarks>
        /// GET api/citizen/FindByInsuranceNumber?insuranceNumber=
        /// </remarks>
        /// <param name="insuranceNumber">insurance number</param>
        /// <returns></returns>
        [HttpGet("FindByInsuranceNumber")]
        public async Task<IActionResult> FindByInsuranceNumberAsync(string insuranceNumber)
        {
            return await FindingByInsuranceNumberAsync(insuranceNumber);
        }

        /// <summary>
        /// Find by taxpayer number
        /// </summary>
        /// <remarks>
        /// GET api/citizen/FindByTaxpayerNumber?taxpayerNumber= 
        /// </remarks>
        /// <param name="taxpayerNumber"></param>
        /// <returns></returns>
        [HttpGet("FindByTaxpayerNumber")]
        public async Task<IActionResult> FindByTaxpayerNumberAsync(string taxpayerNumber)
        {
            return await FindingByTaxpayerNumberAsync(taxpayerNumber);
        }

        /// <summary>
        /// Find by birth date
        /// </summary>
        /// <remarks>
        /// GET api/citizen/FindByBirthDate?birthDate=
        /// </remarks>
        /// <param name="birthDate"></param>
        /// <returns></returns>
        [HttpGet("FindByBirthDate")]
        public async Task<IActionResult> FindByBirthDateAsync(DateTime birthDate)
        {
            return await FindingByBirthDateAsync(birthDate);
        }

        /// <summary>
        /// Find by death date
        /// </summary>
        /// <remarks>
        /// GET api/citizen/FindByDeathDate?deathDate=
        /// </remarks>
        /// <param name="deathDate"></param>
        /// <returns></returns>
        [HttpGet("FindByDeathDate")]
        public async Task<IActionResult> FindByDeathDateAsync(DateTime deathDate)
        {
            return await FindingByDeathDateAsync(deathDate);
        }
        #endregion

        #region Export methods

        /// <summary>
        /// Export by name
        /// </summary>
        /// <remarks>
        /// GET api/citizen/ExportByName?name=
        /// </remarks>
        /// <param name="name"></param>
        /// <returns></returns>
        [HttpGet("ExportByName")]
        [Produces("text/csv")]
        public async Task<IActionResult> ExportByNameAsync(string name)
        {
            return await FindingByNameAsync(name);
        }

        /// <summary>
        /// Export by insurance number
        /// </summary>
        /// <remarks>
        /// GET api/citizen/ExportByInsuranceNumber?insuranceNumber=
        /// </remarks>
        /// <param name="insuranceNumber"></param>
        /// <returns></returns>
        [HttpGet("ExportByInsuranceNumber")]
        [Produces("text/csv")]
        public async Task<IActionResult> ExportByInsuranceNumberAsync(string insuranceNumber)
        {
            return await FindingByInsuranceNumberAsync(insuranceNumber);
        }

        /// <summary>
        /// Export by taxpayer number
        /// </summary>
        /// <remarks>
        /// GET api/citizen/ExportByTaxpayerNumber?taxpayerNumber=
        /// </remarks>
        /// <param name="taxpayerNumber"></param>
        /// <returns></returns>
        [HttpGet("ExportByTaxpayerNumber")]
        [Produces("text/csv")]
        public async Task<IActionResult> ExportByTaxpayerNumberAsync(string taxpayerNumber)
        {
            return await FindingByTaxpayerNumberAsync(taxpayerNumber);
        }

        /// <summary>
        /// Export by birth date
        /// </summary>
        /// <remarks>
        /// GET api/citizen/ExportByBirthDate?birthDate=
        /// </remarks>
        /// <param name="birthDate"></param>
        /// <returns></returns>
        [HttpGet("ExportByBirthDate")]
        [Produces("text/csv")]
        public async Task<IActionResult> ExportByBirthDateAsync(DateTime birthDate)
        {
            return await FindingByBirthDateAsync(birthDate);
        }

        /// <summary>
        /// Export by death date
        /// </summary>
        /// <remarks>
        /// GET api/citizen/ExportByDeathDate?deathDate=
        /// </remarks>
        /// <param name="deathDate"></param>
        /// <returns></returns>
        [HttpGet("ExportByDeathDate")]
        [Produces("text/csv")]
        public async Task<IActionResult> ExportByDeathDateAsync(DateTime deathDate)
        {
            return await FindingByDeathDateAsync(deathDate);
        }
        #endregion

        #region Import methods

        /// <summary>
        /// Imports list of citizens with given input csv rows (with delimiter of comma ',')
        /// </summary>
        /// <remarks>
        /// POST api/Citizen/ImportCitizens
        /// 
        /// Content-Type: text/csv;
        ///
        ///   Иванов,Петр,Семенович,2223334400,444-111-777-44,19.09.1999,
        /// 
        ///   Антонов,Антон,Петрович,1112223344,444-555-777 33,04.04.1908,14.08.2008,
        /// 
        /// </remarks>
        /// <param name="citizens"></param>
        /// <returns>List created ids of citizens.</returns>
        /// <response code="200">Return the newly created citizen</response>
        /// <response code="400">If the citizen is null</response>
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [HttpPost("ImportCitizens")]
        public async Task<IActionResult> ImportCitizens(List<Citizen> citizens)
        {
            try
            {
                if (citizens == null || !citizens.Any())
                {
                    return BadRequest("Invalid model object");
                }

                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }

                await _citizenService.CreateRangeAsync(citizens);

                var citizenIds = citizens.Select(c => c.Id).ToList();
                return Ok(citizenIds);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }
        #endregion

        #region Private methods
        private async Task<IActionResult> FindingByNameAsync(string name)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }

                var citizens = await _citizenService.FindByNameAsync(name);
                if (!citizens.Any())
                {
                    return NoContent();
                }
                return Ok(citizens);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }
        
        private async Task<IActionResult> FindingByInsuranceNumberAsync(string insuranceNumber)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }

                var citizens = await _citizenService.FindByInsuranceNumberAsync(insuranceNumber);
                if (!citizens.Any())
                {
                    return NoContent();
                }
                return Ok(citizens);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        private async Task<IActionResult> FindingByTaxpayerNumberAsync(string taxpayerNumber)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }

                var citizens = await _citizenService.FindByTaxpayerNumberAsync(taxpayerNumber);
                if (!citizens.Any())
                {
                    return NoContent();
                }
                return Ok(citizens);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        private async Task<IActionResult> FindingByBirthDateAsync(DateTime birthDate)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }

                var citizens = await _citizenService.FindByBirthDateAsync(birthDate);
                if (!citizens.Any())
                {
                    return NoContent();
                }
                return Ok(citizens);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        private async Task<IActionResult> FindingByDeathDateAsync(DateTime deathDate)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }

                var citizens = await _citizenService.FindByDeathDateAsync(deathDate);
                if (!citizens.Any())
                {
                    return NoContent();
                }
                return Ok(citizens);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }
        #endregion
    }
}
