﻿using Citizenry.Domain;
using Citizenry.SqlDataAccess;
using Citizenry.WebAPI.Controllers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Controllers;
using System;

namespace Citizenry.WebAPI
{
    public class CitizenControllerActivator : IControllerActivator
    {
        private readonly CitizenConfiguration _configuration;

        public CitizenControllerActivator(CitizenConfiguration configuration)
        {
            if (configuration == null)
            {
                throw new ArgumentNullException(nameof(configuration));
            }

            _configuration = configuration;
        }

        public object Create(ControllerContext context) =>
            this.Create(context.ActionDescriptor.ControllerTypeInfo.AsType());


        public void Release(ControllerContext context, object controller)
            => (controller as IDisposable)?.Dispose();

        public ControllerBase Create(Type type)
        {
            var context = new CitizenContext(_configuration.ConnectionString);

            switch (type.Name)
            {
                case nameof(CitizenController):
                    return new CitizenController(
                        new CitizenService(this.CreateCitizenRepository(context)));

                default:
                    throw new InvalidOperationException($"Unknown controller { type }.");
            }
        }

        private ICitizenRepository CreateCitizenRepository(CitizenContext context) =>
            (ICitizenRepository)Activator.CreateInstance(_configuration.CitizenRepositoryType, context);
    }
}
