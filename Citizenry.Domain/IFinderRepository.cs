﻿using Citizenry.Domain.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Citizenry.Domain
{
    public interface IFinderRepository
    {
        Task<IEnumerable<Citizen>> FindByNameAsync(string name);

        Task<IEnumerable<Citizen>> FindByInsuranceNumberAsync(string insuranceNumber);

        Task<IEnumerable<Citizen>> FindByTaxpayerNumberAsync(string taxpayerNumber);

        Task<IEnumerable<Citizen>> FindByBirthDateAsync(DateTime birthDate);

        Task<IEnumerable<Citizen>> FindByDeathDateAsync(DateTime deathDate);
    }
}
