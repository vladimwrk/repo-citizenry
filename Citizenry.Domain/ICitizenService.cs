﻿using Citizenry.Domain.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Citizenry.Domain
{
    public interface ICitizenService
    {
        Task CreateAsync(Citizen citizen);

        Task CreateRangeAsync(IEnumerable<Citizen> citizens);

        Task UpdateAsync(Citizen citizen);

        Task DeleteAsync(Citizen citizen);

        Task<Citizen> GetCitizenByIdAsync(int id);

        Task<IEnumerable<Citizen>> GetAllCitizensAsync();

        #region Find methods

        Task<IEnumerable<Citizen>> FindByNameAsync(string name);

        Task<IEnumerable<Citizen>> FindByInsuranceNumberAsync(string number);

        Task<IEnumerable<Citizen>> FindByTaxpayerNumberAsync(string number);

        Task<IEnumerable<Citizen>> FindByBirthDateAsync(DateTime birthDate);

        Task<IEnumerable<Citizen>> FindByDeathDateAsync(DateTime deathDate);
        #endregion
    }
}
