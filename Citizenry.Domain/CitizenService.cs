﻿using Citizenry.Domain.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Citizenry.Domain
{
    public class CitizenService : ICitizenService
    {
        private readonly ICitizenRepository _repository;

        public CitizenService(ICitizenRepository repository)
        {
            if (repository == null)
            {
                throw new ArgumentNullException(nameof(repository));
            }

            _repository = repository;
        }

        public async Task CreateAsync(Citizen citizen)
        {
            await _repository.CreateAsync(citizen);
        }

        public async Task CreateRangeAsync(IEnumerable<Citizen> citizens)
        {
            await _repository.CreateRangeAsync(citizens);
        }

        public async Task UpdateAsync(Citizen citizen)
        {
            await _repository.UpdateAsync(citizen);
        }

        public async Task DeleteAsync(Citizen citizen)
        {
            await _repository.DeleteAsync(citizen);
        }

        public async Task<Citizen> GetCitizenByIdAsync(int id)
        {
            return await _repository.GetCitizenByIdAsync(id);
        }

        public async Task<IEnumerable<Citizen>> GetAllCitizensAsync()
        {
            return await _repository.GetAllCitizensAsync();
        }

        #region Find methods

        public async Task<IEnumerable<Citizen>> FindByNameAsync(string name)
        {
            return await _repository.FindByNameAsync(name);
        }

        public async Task<IEnumerable<Citizen>> FindByInsuranceNumberAsync(string number)
        {
            return await _repository.FindByInsuranceNumberAsync(number);
        }

        public async Task<IEnumerable<Citizen>> FindByTaxpayerNumberAsync(string number)
        {
            return await _repository.FindByTaxpayerNumberAsync(number);
        }

        public async Task<IEnumerable<Citizen>> FindByBirthDateAsync(DateTime birthDate)
        {
            return await _repository.FindByBirthDateAsync(birthDate);
        }

        public async Task<IEnumerable<Citizen>> FindByDeathDateAsync(DateTime deathDate)
        {
            return await _repository.FindByDeathDateAsync(deathDate);
        }
        #endregion
    }
}