﻿using Citizenry.Domain.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Citizenry.Domain
{
    public interface ICitizenRepository : IFinderRepository
    {
        Task CreateAsync(Citizen citizen);

        Task CreateRangeAsync(IEnumerable<Citizen> citizens);

        Task UpdateAsync(Citizen citizen);

        Task DeleteAsync(Citizen citizen);
        
        Task<Citizen> GetCitizenByIdAsync(int id);

        Task<IEnumerable<Citizen>> GetAllCitizensAsync();
    }
}
