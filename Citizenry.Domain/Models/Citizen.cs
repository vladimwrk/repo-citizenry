﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Citizenry.Domain.Models
{
    public class Citizen
    {
        public Citizen()
        {

        }

        public int Id { get; set; }

        [Required(ErrorMessage = "LastName is required.")]
        [StringLength(50)]
        public string LastName { get; set; }

        [Required(ErrorMessage = "FirstName is required.")]
        [StringLength(50)]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "MiddleName is required.")]
        [StringLength(50)]
        public string MiddleName { get; set; }

        /*
         * Тип, используемый для представления 12-значного ИНН физического лица или
         * 10-значного ИНН юридического лица
         */

        /// <summary>
        /// Indi­vid­ual Tax­pay­er Num­ber
        /// </summary>
        [Required(ErrorMessage = "TaxpayerNumber is required.")]
        [StringLength(12)]
        [RegularExpression(@"^(\d{10}|\d{12})$")]
        public string TaxpayerNumber { get; set; }

        /*
         * Формат СНИЛС: «ХХХ-ХХХ-ХХХ YY», где X,Y — цифры, причём первые девять цифр 'X' — это любые
         * цифры, а последние две 'Y' фактически являются контрольной суммой, вычисляемой по особому
         * алгоритму из последовательности первых 9 цифр.
         */

        /// <summary>
        /// Insurance Number of Individual Ledger Account
        /// </summary>
        [Required(ErrorMessage = "InsuranceNumber is required.")]
        [StringLength(14)]
        [RegularExpression(@"^(\d{3}-\d{3}-\d{3})(\s\d{2}|-\d{2})$")]
        public string InsuranceNumber { get; set; }

        [Required(ErrorMessage = "BirthDate is required.")]
        [DataType(DataType.Date)]
        public DateTime BirthDate { get; set; }

        [DataType(DataType.Date)]
        public DateTime? DeathDate { get; set; }
    }
}
