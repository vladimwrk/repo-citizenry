﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Formatters;
using Microsoft.Net.Http.Headers;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SampleTextFormatters
{
    public class CsvTextOutputFormatter : TextOutputFormatter
    {
        public CsvTextOutputFormatter()
        {
            SupportedMediaTypes.Add(MediaTypeHeaderValue.Parse("text/csv"));
            SupportedEncodings.Add(Encoding.UTF8);
            SupportedEncodings.Add(Encoding.Unicode);
        }

        public string CsvSeparator { get; set; } = ",";

        public override Task WriteResponseBodyAsync(
            OutputFormatterWriteContext context,
            Encoding selectedEncoding)
        {
            StringBuilder sb = new StringBuilder();
            Type type = GetTypeOf(context.Object);

            if (type == null)
            {
                return Task.Run(() => new ArgumentNullException("Value is null."));
            }

            sb.AppendLine(string.Join(CsvSeparator, type.GetProperties()
                .Where(pi => !pi.Name.Equals("IsDeleted", StringComparison.CurrentCultureIgnoreCase))
                .Select(pi => pi.Name)));

            foreach (var obj in (IEnumerable<object>)context.Object)
            {
                var values = obj.GetType().GetProperties()
                    .Where(pi => !pi.Name.Equals("IsDeleted", StringComparison.CurrentCultureIgnoreCase))
                    .Select(pi => new { Value = pi.GetValue(obj, null) });

                List<string> listValues = new List<string>();

                foreach (var val in values)
                {
                    if (val.Value != null)
                    {
                        var tempVal = val.Value.ToString();

                        if (tempVal.Contains(CsvSeparator))
                        {
                            tempVal = string.Concat("\"", tempVal, "\"");
                        }
                        tempVal.Replace("\r", string.Empty, StringComparison.InvariantCultureIgnoreCase);
                        tempVal.Replace("\n", string.Empty, StringComparison.InvariantCultureIgnoreCase);

                        listValues.Add(tempVal);
                    }
                    else
                    {
                        listValues.Add(string.Empty);
                    }
                }

                sb.AppendLine(string.Join(",", listValues));
            }

            return context.HttpContext.Response.WriteAsync(sb.ToString(), selectedEncoding);
        }

        private static Type GetTypeOf(object obj)
        {
            Type type = obj.GetType();
            Type resultType;

            if (type.GetGenericArguments().Length > 0)
            {
                resultType = type.GetGenericArguments()[0];
            }
            else
            {
                resultType = type.GetElementType();
            }

            return resultType;
        }

        protected override bool CanWriteType(Type type)
        {
            foreach (Type interfaceType in type.GetInterfaces())
            {
                if (interfaceType == typeof(IEnumerable))
                    return true;
            }

            return false;
        }
    }
}
