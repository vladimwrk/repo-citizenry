﻿using Microsoft.AspNetCore.Mvc.Formatters;
using Microsoft.Net.Http.Headers;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace SampleTextFormatters
{
    public class CsvTextInputFormatter : TextInputFormatter
    {
        public CsvTextInputFormatter()
        {
            SupportedMediaTypes.Add(MediaTypeHeaderValue.Parse("text/csv"));
            SupportedEncodings.Add(Encoding.UTF8);
            SupportedEncodings.Add(Encoding.Unicode);
        }

        public string CsvSeparator { get; set; } = ",";

        public override Task<InputFormatterResult> ReadRequestBodyAsync(
            InputFormatterContext context,
            Encoding encoding)
        {
            var type = context.ModelType;
            var request = context.HttpContext.Request;

            var result = ReadStream(type, request.Body, encoding);
            return InputFormatterResult.SuccessAsync(result);
        }

        public object ReadStream(Type type, Stream stream, Encoding encoding)
        {
            Type itemType;
            IList list;
            var typeIsArray = false;

            if (type.GetGenericArguments().Length > 0)
            {
                itemType = type.GetGenericArguments()[0];
                list = (IList)Activator.CreateInstance(type);
            }
            else
            {
                typeIsArray = true;
                itemType = type.GetElementType();

                var listType = typeof(List<>);
                var constructedListType = listType.MakeGenericType(itemType);
                list = (IList)Activator.CreateInstance(constructedListType);
            }

            using (var sr = new StreamReader(stream, encoding))
            {
                try
                {
                    while (!sr.EndOfStream)
                    {
                        var line = sr.ReadLine();

                        if (line != null)
                        {
                            var values = line.Split(CsvSeparator);

                            var itemTypeInGeneric = list.GetType().GetTypeInfo().GenericTypeArguments[0];
                            var item = Activator.CreateInstance(itemTypeInGeneric);
                            var properties = item.GetType().GetProperties()
                                .Where(p => !p.Name.Equals("Id", StringComparison.CurrentCultureIgnoreCase))
                                .ToArray();

                            for (int i = 0; i < values.Length; i++)
                            {
                                try
                                {
                                    Type t = Nullable.GetUnderlyingType(properties[i].PropertyType) ??
                                             properties[i].PropertyType;
                                    object safeValue = (values[i] == null) ? null : Convert.ChangeType(values[i], t);

                                    properties[i].SetValue(item, safeValue, null);
                                }
                                catch (Exception ex)
                                {
                                    //log
                                    values[i] = default;
                                    continue;
                                }
                            }
                            list.Add(item);
                        }
                    }

                    if (typeIsArray)
                    {
                        Array array = Array.CreateInstance(itemType, list.Count);
                        for (int i = 0; i < list.Count; i++)
                        {
                            array.SetValue(list[i], i);
                        }

                        return array;
                    }
                }
                catch (Exception ex)
                {
                    //log
                }
                return list;
            }
        }

        public override bool CanRead(InputFormatterContext context)
        {
            var type = context.ModelType;
            if (type == null)
            {
                throw new ArgumentNullException(nameof(type));
            }

            return IsTypeOfEnumerable(type);
        }

        private bool IsTypeOfEnumerable(Type type)
        {
            foreach (Type interfaceType in type.GetInterfaces())
            {
                if (interfaceType == typeof(IList))
                    return true;
            }

            return false;
        }
    }
}
