﻿using System;
using Citizenry.Domain.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Citizenry.SqlDataAccess
{
    public class CitizenModelConfiguration : IEntityTypeConfiguration<Citizen>
    {
        public void Configure(EntityTypeBuilder<Citizen> builder)
        {
            builder.ToTable("Citizens");
            builder.Property(p => p.LastName).IsRequired().HasMaxLength(50);
            builder.Property(p => p.FirstName).IsRequired().HasMaxLength(50);
            builder.Property(p => p.MiddleName).IsRequired().HasMaxLength(50);

            builder.Property(p => p.TaxpayerNumber).IsRequired().HasMaxLength(12);
            builder.Property(p => p.InsuranceNumber).IsRequired().HasMaxLength(14);

            builder.Property(p => p.BirthDate).IsRequired().HasColumnType("date");
            builder.Property(p => p.DeathDate).IsRequired(false).HasColumnType("date");

            builder.HasData(
                new Citizen
                {
                    Id = 1,
                    LastName = "Сидоров",
                    FirstName = "Максим",
                    MiddleName = "Федорович",
                    TaxpayerNumber = "240850327222",
                    InsuranceNumber = "111-222-345-22",
                    BirthDate = new DateTime(1977, 01, 01)
                },
                new Citizen()
                {
                    Id = 2,
                    LastName = "Ляпкин",
                    FirstName = "Амос",
                    MiddleName = "Федорович",
                    TaxpayerNumber = "0123456789",
                    InsuranceNumber = "123-123-345-11",
                    BirthDate = new DateTime(1947, 05, 05)
                },
                new Citizen()
                {
                    Id = 3,
                    LastName = "Хлестаков",
                    FirstName = "Иван",
                    MiddleName = "Александрович",
                    
                    TaxpayerNumber = "012345678933",
                    InsuranceNumber = "111-233-345 33",
                    BirthDate = new DateTime(1827, 02, 02),
                    DeathDate = new DateTime(1927, 04, 04)
                },
                new Citizen()
                {
                    Id = 4,
                    LastName = "Павлов",
                    FirstName = "Павел",
                    MiddleName = "Павлович",
                    TaxpayerNumber = "012345678944",
                    InsuranceNumber = "12-233-235 44",
                    BirthDate = new DateTime(1987, 03, 03)
                },
                new Citizen()
                {
                    Id = 5,
                    LastName = "Родионов",
                    FirstName = "Федор",
                    MiddleName = "Михайлович",
                    TaxpayerNumber = "012345678955",
                    InsuranceNumber = "112-233-125 55",
                    BirthDate = new DateTime(1977, 04, 04)
                },
                new Citizen()
                {
                    Id = 7,
                    LastName = "Носов",
                    FirstName = "Николай",
                    MiddleName = "Иванович",
                    TaxpayerNumber = "012345678977",
                    InsuranceNumber = "123-123-123-77",
                    BirthDate = new DateTime(1907, 05, 05),
                    DeathDate = new DateTime(2007, 06, 06)
                }
            );
        }
    }
}
