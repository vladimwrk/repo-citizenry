﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Citizenry.SqlDataAccess.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Citizens",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    LastName = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false),
                    FirstName = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false),
                    MiddleName = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false),
                    TaxpayerNumber = table.Column<string>(type: "nvarchar(12)", maxLength: 12, nullable: false),
                    InsuranceNumber = table.Column<string>(type: "nvarchar(14)", maxLength: 14, nullable: false),
                    BirthDate = table.Column<DateTime>(type: "date", nullable: false),
                    DeathDate = table.Column<DateTime>(type: "date", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Citizens", x => x.Id);
                });

            migrationBuilder.InsertData(
                table: "Citizens",
                columns: new[] { "Id", "BirthDate", "DeathDate", "FirstName", "InsuranceNumber", "LastName", "MiddleName", "TaxpayerNumber" },
                values: new object[,]
                {
                    { 1, new DateTime(1977, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "Максим", "111-222-345-22", "Сидоров", "Федорович", "240850327222" },
                    { 2, new DateTime(1947, 5, 5, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "Амос", "123-123-345-11", "Ляпкин-Тяпкин", "Федорович", "012123456111" },
                    { 3, new DateTime(1827, 2, 2, 0, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(1927, 4, 4, 0, 0, 0, 0, DateTimeKind.Unspecified), "Иван", "111-233-345 33", "Хлестаков", "Александрович", "240850372333" },
                    { 4, new DateTime(1987, 3, 3, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "Павел", "12-233-235 44", "Павлов", "Павлович", "240853452444" },
                    { 5, new DateTime(1977, 4, 4, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "Федор", "112-233-125 55", "Родионов", "Михайлович", "123455789555" },
                    { 7, new DateTime(1907, 5, 5, 0, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(2007, 6, 6, 0, 0, 0, 0, DateTimeKind.Unspecified), "Николай", "123-123-123-77", "Носов", "Иванович", "012123456777" }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Citizens");
        }
    }
}
