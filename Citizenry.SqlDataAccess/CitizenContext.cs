﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Citizenry.Domain.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Citizenry.SqlDataAccess
{
    public class CitizenContext : RepositoryContext
    {
        private readonly string _connectionString;

        public CitizenContext()
        {
        }

        public CitizenContext(string connectionString)
        {
            if (connectionString == null)
            {
                throw new ArgumentNullException(nameof(connectionString));
            }

            if (string.IsNullOrWhiteSpace(connectionString))
            {
                throw new ArgumentException("Value should not be empty.", nameof(connectionString));
            }

            _connectionString = connectionString;
        }

        public DbSet<Citizen> Citizens { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(_connectionString);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new CitizenModelConfiguration());
        }
    }

    public class RepositoryContext : DbContext
    {

    }
}
