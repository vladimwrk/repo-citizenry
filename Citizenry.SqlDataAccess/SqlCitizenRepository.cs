﻿using Citizenry.Domain;
using Citizenry.Domain.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Citizenry.SqlDataAccess
{
    public class SqlCitizenRepository : CitizenRepositoryBase<Citizen>, ICitizenRepository
    {
        public SqlCitizenRepository(CitizenContext context)
            : base(context)
        {
        }

        public async Task CreateAsync(Citizen citizen)
        {
            Create(citizen);
            await this.Context.SaveChangesAsync();
        }

        public async Task CreateRangeAsync(IEnumerable<Citizen> citizens)
        {
            CreateRange(citizens);
            await this.Context.SaveChangesAsync();
        }

        public async Task UpdateAsync(Citizen citizen)
        {
            Update(citizen);
            await this.Context.SaveChangesAsync();
        }

        public async Task DeleteAsync(Citizen citizen)
        {
            Delete(citizen);
            await this.Context.SaveChangesAsync();
        }

        public async Task<Citizen> GetCitizenByIdAsync(int id)
        {
            return await FindByCondition(c => c.Id.Equals(id)).FirstOrDefaultAsync();
        }

        public async Task<IEnumerable<Citizen>> GetAllCitizensAsync()
        {
            return await FindAll()
                .OrderBy(c => c.LastName)
                .ToListAsync();
        }
        
        #region Find Methods

        public async Task<IEnumerable<Citizen>> FindByNameAsync(string name)
        {
            var query = FindByCondition(c => c.LastName.Contains(name)
                                                || c.FirstName.Contains(name)
                                                || c.MiddleName.Contains(name));
            return await query.ToListAsync();
        }

        public async Task<IEnumerable<Citizen>> FindByInsuranceNumberAsync(string insuranceNumber)
        {
            return await FindByCondition(c => c.InsuranceNumber.Contains(insuranceNumber))
                .ToListAsync();
        }

        public async Task<IEnumerable<Citizen>> FindByTaxpayerNumberAsync(string taxpayerNumber)
        {
            return await FindByCondition(c => c.TaxpayerNumber.Contains(taxpayerNumber))
                .ToListAsync();
        }

        public async Task<IEnumerable<Citizen>> FindByBirthDateAsync(DateTime birthDate)
        {
            return await FindByCondition(c => c.BirthDate.CompareTo(birthDate) == 0)
                .ToListAsync();
        }

        public async Task<IEnumerable<Citizen>> FindByDeathDateAsync(DateTime deathDate)
        {
            return await FindByCondition(c => c.DeathDate.HasValue && 
                                              c.DeathDate.Value.CompareTo(deathDate) == 0)
                .ToListAsync();
        }
        #endregion
    }
}
