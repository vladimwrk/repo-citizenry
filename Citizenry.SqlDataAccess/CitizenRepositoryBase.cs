﻿using Citizenry.Domain;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Citizenry.SqlDataAccess
{
    public abstract class CitizenRepositoryBase<T> : ICitizenRepositoryBase<T> where T : class
    {
        protected CitizenRepositoryBase(RepositoryContext context)
        {
            if (context == null)
            {
                throw new ArgumentNullException(nameof(context));
            }

            Context = context;
        }

        protected RepositoryContext Context { get; set; }

        public void Create(T entity)
        {
            Context.Set<T>().Add(entity);
        }

        public void CreateRange(IEnumerable<T> entities)
        {
            Context.Set<T>().AddRange(entities);
        }

        public void Update(T entity)
        {
            Context.Set<T>().Update(entity);
        }

        public void Delete(T entity)
        {
            Context.Set<T>().Remove(entity);
        }

        public IQueryable<T> FindAll()
        {
            return Context.Set<T>().AsNoTracking();
        }

        public IQueryable<T> FindByCondition(Expression<Func<T, bool>> expression)
        {
            return Context.Set<T>().Where(expression).AsNoTracking();
        }
    }
}
